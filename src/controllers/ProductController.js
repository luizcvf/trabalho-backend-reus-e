const Product = require('../models/Product');
const User = require('../models/User');

async function create(req,res) {
    try{
        const product = await Product.create(req.body);
        return res.status(200).json({message: "Produto cadastrado com sucesso", Product: product});
    }catch(error) {
        return res.status(500).json(error);
    }
};

async function index (req,res){
    try {
        const products = await Product.findAll();
        return res.status(200).json({message: "Todos os produtos listados", Products: products});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function show (req, res) {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({message: "Produto encontrado", Product: product});
    } catch (e) {
        return res.status(500).json(e)
    }
}

async function update (req,res){
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated){
            const product = await Product.findByPk(id);
            return res.status(200).json(product);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("Produto não encontrado");
    }
}

async function destroy (req,res){
    const {id} = req.params;
    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Produto deletado");
        }
        throw new Error();
    } catch (e) {
        return res.status(500).json("Produto não encontrado");
    }
}

async function addUser (req, res) {
    const {userId, productId} = req.params;
    try {
        const user = await User.findByPk(userId);
        const product = await Product.findByPk(productId);
        await product.setUser(user);
        return res.status(200).json(product);
    } catch (error) {
        return res.status(500).json({error});
    }
}

const removeUser = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setUser(null);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json(err + "!");
    }
};

module.exports = {
    create,
    index,
    update,
    show,
    destroy,
    addUser,
    removeUser,
}
