const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define("User", {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  phoneNumber: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  adress: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  photo: {
    type: DataTypes.STRING,
    allowNull: false
  },
  cpf: {
    type: DataTypes.NUMBER,
    allowNull: false
  }
}, {timestamps: true});


User.associate = (models) => {
    User.hasMany(models.Product);
};

module.exports = User