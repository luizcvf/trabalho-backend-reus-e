const Express = require('express');
const router = Express();
const UserController = require('../controllers/UserController');
const ProductController = require('../controllers/ProductController');

router.post('/user', UserController.create);
router.get('/user/:id', UserController.show);
router.get('/user', UserController.index);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.destroy);

router.post('/product', ProductController.create);
router.get('/product/:id', ProductController.show);
router.get('/product', ProductController.index);
router.put('/product/:id', ProductController.update);
router.delete('/product/:id', ProductController.destroy);

router.put('/productremove/:id', ProductController.removeUser);
router.put('/user/:userId/product/:productId', ProductController.addUser);

module.exports = router;
